# Docker Image Rebuilder

– A handy tool for reproducibly building Docker/OCI images.

## Quickstart

Run the following on a Linux system (not tested anywhere else, help wanted!):

 1. Clone this repository: `git clone https://gitlab.com/ntninja/docker-image-rebuilder.git`
 2. Change working directory: `cd docker-image-rebuilder`
 3. Do one of the following to install the required depdencies:
    a. Run the installation script: `./prepare.sh`
       * Recommended! This will ask for your password if needed – just look at the source if you don't trust me.
    b. Ensure you have Python 3 and PIP installed and run: `pip3 install -r requirements.txt`
 4. Done!

## Usage

Unfortunately there is no tutorial here yet, but you can get a good feel for how to use this script by a look at

 * [https://gitlab.com/ntninja/docker-recipies](https://gitlab.com/ntninja/docker-recipies)

Each directory contains a Dockerfile to be rebuilt and `docker-recipies.toml` contains the list of all available files as well as some extra data.

## The configuration file

In order to use `docker-image-rebuilder` a configuration file must be provided. The following options are supported:

 * `state-dir`: The directory where `docker-image-rebuilder` will persist information (checksums) between builds
 * `files`: A mapping of images to build (single images can be selected for build using the `-b` command-line option):
    * `path`: Path to the directory containing the `Dockerfile` to use for rebuilding the image
    * `tags`: Docker tags to push the successfully built image as
    * `volatile-files`: List of filepaths to ignore when trying to determine whether the respective image changed
       * Filepaths may be Glob patterns; see [the `globre` documentation](https://github.com/metagriffin/globre/blob/master/README.rst#details) for details and examples
    * `dockerfile`: Dockerfile name (relative to `path`).  Defaults to `Dockerfile`
    * `pre-commands`: List of shell command-strings to execute (in order) before starting the build
    * `post-commands`: List of shell command-strings to execute (in order) after successfully finishing a build with changes
    * `group`: A String to specify grouping of images to build (to allow building a subset of images).  Optional
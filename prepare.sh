#!/bin/bash
set -eu -o pipefail

HAVE_PYTHON3=true
SYSTEM_CHANGED=false
APT_INSTALLS=()
PIP_INSTALLS=()

# Check for Python 3 itself (and assume nothing else required is installed if this isn't)
if ! type python3 2>/dev/null >&2;
then
	APT_INSTALLS+=(python3)
fi

# Check for the `docker` Python module
if ! ${HAVE_PYTHON3} || ! python3 -c "import docker" 2>/dev/null >&2;
then
	APT_INSTALLS+=(python3-docker)
fi

# Check for the `toml` Python module
if ! ${HAVE_PYTHON3} || ! python3 -c "import toml" 2>/dev/null >&2;
then
	APT_INSTALLS+=(python3-toml)
fi

# Check for the `globre` Python module
if ! ${HAVE_PYTHON3} || ! python3 -c "import globre" 2>/dev/null >&2;
then
	PIP_INSTALLS+=(globre)
fi

# Check for PIP if we need for installation
if ! ${HAVE_PYTHON3} || ( [ ${#PIP_INSTALLS[@]} -gt 0 ] && ! type pip3 2>/dev/null >&2 );
then
	APT_INSTALLS+=(python3-pip)
fi

# Install system packages
if [ ${#APT_INSTALLS[@]} -gt 0 ];
then
	if [ ${#APT_INSTALLS[@]} -gt 0 ] && type apt 2>/dev/null >&2;
	then
		sudo apt install ${APT_INSTALLS[@]}
		SYSTEM_CHANGED=true
	else
		#TODO: Insert support for other package managers (zypper, dnf, apk, …) here
		echo "No supported packages manager found!" >&2
		echo "Please install the following packages manually: ${APT_INSTALLS[*}" >&2
		exit 1
	fi
fi

# Install PIP packages
if [ ${#PIP_INSTALLS[@]} -gt 0 ];
then
	sudo pip3 install ${PIP_INSTALLS[@]}
	SYSTEM_CHANGED=true
fi

if ${SYSTEM_CHANGED};
then
	echo
	echo -n "Success! "
else
	echo -n "No system changes were required! "
fi
echo "You should now be able to run \`./docker-image-rebuilder\`."